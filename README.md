# Jessica dokuwiki template

* Designed by [Jessica Russell Flint](http://jessicarussellflint.co.uk) and converted by desbest[http://desbest.com]
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:jessica)

![jessica theme screenshot](https://i.imgur.com/hSShJUx.png)